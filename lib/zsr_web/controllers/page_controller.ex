defmodule ZSRWeb.PageController do
  use ZSRWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end

  def design(conn, _params) do
    render conn, "design.html"
  end
end
